
# VLBI2JSON Converter

VLBI2JSON Converter is a tool designed to convert VLBI experiment files (VEX format) into JSON format based on a provided JSON template. This tool allows for conversion from VEX file to JSON file mainly for SKAMPI telescope. Contact jompoj@mpifr-bonn.mpf.de for further requests. 

## Features

- Converts VEX files to JSON format.
- Supports custom JSON templates.
- Ensures station names are valid before processing.
- Provides detailed output for each scan.

## Prerequisites

- Python 3.x
- Required Python packages
  - `re`
  - `copy`
  - `pandas`
  - `astropy`
  - `argparse`
  - `datetime`
  - `json`

## Installation

1. Clone the repository:
    ```bash
    git clone https://gitlab.mpcdf.mpg.de/jompoj/vex2jsonforskampi.git
    cd vex2jsonforskampi
    ```

2. Install the required packages:
    ```bash
    pip install pandas astropy
    ```

## Usage

1. Create a JSON template file (e.g., `template.json`). Ensure it has the necessary structure. (No need for SKAMPI)

2. Run the script with the required arguments:
    ```bash
    python vlbi2json_converter.py  --jsonname OUTPUT_JSON_NAME STATION_NAME VEX_FILE_PATH 

    ```

    - `STATION_NAME`: Name of the station to filter.
    - `VEX_FILE_PATH`: Path to the VEX file.
    - `JSON_TEMPLATE_PATH` (optional): Path to the JSON template file. (No need to specify for SKAMPI-Sband)
    - `OUTPUT_JSON_NAME` (optional): Name of the output JSON file (default: `sample.json`).


### Example

```bash
   python vex2json.py --jsonname Skout.json Sf example/te019.vex 
```


## Acknowledgments

- [pyvexfile](hhttps://github.com/bmarcote/vex) by Benito Marcote.
