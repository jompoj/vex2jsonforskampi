#!/usr/bin/env python
# coding: utf-8

# # VEXtoJSON test 
# Local packages

import pyvexfile as vex
# Call packages 
import re
from copy import deepcopy
import pandas as pd 
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.time import Time
from datetime import datetime, timedelta
import json
import copy
import argparse
import os 

# Convert timestring to astropy ISO format
def convert_to_iso(timestamp_str):
    # Extract components from the string
    year = int(timestamp_str[:4])
    day_of_year = int(timestamp_str[5:8])
    hour = int(timestamp_str[9:11])
    minute = int(timestamp_str[12:14])
    second = int(timestamp_str[15:17])
    start_of_year = datetime(year, 1, 1)
    # Add the day of the year to the start_of_year to get the correct date
    correct_date = start_of_year + timedelta(days=day_of_year - 1, hours=hour, minutes=minute, seconds=second)
    # Format the datetime object into a standard format (ISO 8601)
    return Time(correct_date.isoformat(), format='isot', scale='utc')

# Function to read VEX file and extract schedule information
def getsch(vexname):
    v = vex.Vex(vexname)
    v.from_file(vexname)
    # Assuming section is your pyvexfile.Section object
    section = v.sections['SCHED']
    # Define the regular expressions to extract information
    start_pattern = re.compile(r'start\s*=\s*([\d\w]+)')
    source_pattern = re.compile(r'source\s*=\s*([\d\w.+-]+)')
    station_pattern = re.compile(r'station\s*=\s*(\w+):\s*\d+\s*sec:\s*(\d+)\s*sec')
    # Create a list to hold the extracted data
    data = []
    # Loop over the items in the section
    for key, value in section.items():
        # Convert the value to a string
        value_str = str(value)
        # Extract start time
        start_match = start_pattern.search(value_str)
        start_time = start_match.group(1) if start_match else None
        # Convert start time to ISO format
        start_time_iso = convert_to_iso(start_time) if start_time else None
        # Extract source name
        source_match = source_pattern.search(value_str)
        source_name = source_match.group(1) if source_match else None
        # Extract station names and durations
        station_matches = station_pattern.findall(value_str)
        # Create an entry for each station
        for station, duration in station_matches:
            data.append({
                'source_name': source_name,
                'start_time': start_time_iso,
                'duration': int(duration),  # Convert duration to integer
                'station_name': station
            })
    # Convert the list of dictionaries to a pandas DataFrame
    df = pd.DataFrame(data)
    return df, v
    
# Function to get full schedule with additional information
def get_fullsch(vexname):
    # Load VEX file and get pandas DataFrame with schedule
    data, vex = getsch(vexname)
    source = vex.sections['SOURCE']
    davex = []
    for key, value in source.items():
        print(key, value)
        davex.append(value)
    test_dict = {}
    s = []
    # Extracting source name, RA, DEC from VEX entries 
    for num in range(0, len(davex)):
        try:
            for value in davex[num]:
                value_string = str(davex[num][value])
                if value_string[0] != '*':
                    line = value_string.splitlines()
                    for i in line:
                        for j in i.split(';'):
                            entry = j.strip(" ").split('=')
                            try:
                                test_dict[entry[0].strip(" ")] = str(entry[1]).strip(" ")
                            except:
                                pass
            s.append(deepcopy(test_dict))
            test_dict.clear()
            print(s[-1])
        except:
            pass

    # Convert from d,m,s format to d,m,s format in DEC 
    for source in s:
        source.update({"dec": source['dec'].replace("\'", "m").replace("\"", "s")})
    source = pd.DataFrame.from_dict(s)
    # Convert to astropy time
    c = SkyCoord(source['ra'], source['dec'], unit=(u.hourangle, u.deg), frame='icrs')
    source['RA_deg'] = c.ra.deg
    source['DEC_deg'] = c.dec.deg
    RA = []
    DEC = []
    print(data)
    # Add RA, DEC to schedule DataFrame
    for i in data['source_name']:
        print(source.loc[source['source_name'] == i]['RA_deg'].values,i)
        try: 
                RA.append(source.loc[source['source_name'] == i]['RA_deg'].values[0])
                DEC.append(source.loc[source['source_name'] == i]['DEC_deg'].values[0])
        except IndexError:
                #print("Warning, cant detect either RA or DEC, please make sure to put that by hands")
                #print(i) 
                RA.append(999)
                DEC.append(999)
    print(len(data))
    data['RA'] = RA
    data['DEC'] = DEC
    data['end_time'] = data['start_time'] + data['duration'] / (24.0 * 3600)
    return data

# Function to load a JSON template from a file
def load_json_template(file_path):
    """
    Load a JSON template from a file.

    Parameters:
    file_path (str): The path to the JSON file.

    Returns:
    dict: The loaded JSON template as a dictionary.
    """
    try:
        with open(file_path, 'r') as file:
            template = json.load(file)
    except FileNotFoundError:
        print(f"Error: The file at {file_path} was not found.")
        template = {
            "band": "SBAND",
            "mode": "VLBI",
            "start_pos_accuracy": 0.05,
            "project_id": "None",
            "program_track_delay": 2,
            "estimate_reprovision_duration": 120,
            "force_queue_removal": False,
            "starttime": "",
            "endtime": "",
            "scans": []
        }
    except json.JSONDecodeError:
        print("Error: Failed to decode JSON. Ensure the file contains valid JSON.")
    return template

# Function to write JSON output based on station and VEX file
def write_json(station, vexname, json_template_path, jsonname='sample.json'):    
    # Load the JSON template from the specified file
    json_template = load_json_template(json_template_path)
    
    # Generate the JSON data
    json_back = []
    
    df_all_stations=copy.deepcopy(get_fullsch(vexname))
    while True:
        df_filtered = df_all_stations[df_all_stations['station_name'] == station]
        if len(df_filtered) == 0:
            print('It seems like your station name is not valid. Here are the available station names:')
            print(df_all_stations['station_name'].unique())
            station = input('Please enter a valid station name: ')
        else:
            df = df_filtered
            break

    for index, row in df.iterrows():
        json_entry = copy.deepcopy(json_template)
        print(json_entry['starttime'])
        json_entry["starttime"] = row['start_time'].value
        json_entry["endtime"] = row['end_time'].value
        
        scan = {
            "time_step": 0.5,
            "duration": row['duration'],
            "scan_type": "track",
            "ra": row['RA'],
            "dec": row['DEC'],
            "source_name": row['source_name']
        }
    
        json_entry["scans"].append(scan)
        json_back.append(json_entry)
    
    # Print the resulting JSON structure
    print(json.dumps(json_back, indent=4))
    
    # Save the resulting JSON structure to a file
    json_object = json.dumps(json_back, indent=4)
    with open(jsonname, "w") as outfile:
        outfile.write(json_object)
    return df


# Main function to handle command-line arguments and call write_json
def main():
    parser = argparse.ArgumentParser(description='Convert VEX file to JSON based on a template.')
    parser.add_argument('station', type=str, help='Station name')
    parser.add_argument('vexname', type=str, help='VEX file name')
    parser.add_argument('--json_template_path', type=str, default=' ', help='Path to the JSON template file')
    parser.add_argument('--jsonname', type=str, default='sample.json', help='Output JSON file name (default: sample.json)')
    
    args = parser.parse_args()
        # Check if the vexname file exists
    if not os.path.isfile(args.vexname):
        print(f"Error: The VEX file '{args.vexname}' does not exist.")
        return
    
    write_json(args.station, args.vexname, args.json_template_path, args.jsonname)

if __name__ == '__main__':
    main()
